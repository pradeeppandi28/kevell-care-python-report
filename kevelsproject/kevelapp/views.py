from datetime import date, datetime


# Create your views here.
from rest_framework.response import Response
import pymongo
from rest_framework.views import APIView
import json
from bson.json_util import dumps
from torch import int32

client = pymongo.MongoClient('localhost', 27017)
db = client['kevellcare1']
patient_register = db['PatientRegistration']
Patient_visit = db['Patientvisited']

# Get the patient personal details return respons


class GetAllPatientRegistrations(APIView):
    def post(self, request):

        mylist = []
        i = 0

        for x in patient_register.find({}):

            i = i+1

            my_result = {}
            my_result["Sno"] = i
            my_result["Id"] = x['_id']
            my_result["RegDate"] = x['patientinfo']['RegDate']
            my_result["PatientId"] = 'P'+str(x['_id'])
            my_result["PatientName"] = x['patientinfo']['FirstName']
            my_result["Age"] = x['patientinfo']['Age']
            my_result["AadharNo"] = x['patientinfo']['AadharNo']
            my_result["Gender"] = x['patientinfo']['Gender']
            my_result["MobileNo"] = x['patientinfo']['CellPhoneNo']
            my_result["Diagnosis"] = ""

            mylist.append(my_result)

        return Response({
            "status": "true",
            "responsecode": 200,
            "message": "Success",
            "data": mylist
        })


# Get the patient and doctor particuler data return response
class GetAppointmentData(APIView):
    def post(self, request):

        count = patient_register.count()
        # print("count", count)

        #doctornameid = 1020
        #doctorpractitionerid = 1000
        #appointmentdateforstring = "2022-09-20"

        my_query = {}

        try:
            if request.data['SelectedDoctorId'] == 0:
                doctornameid = ""
            else:
                doctornameid = request.data['SelectedDoctorId']
                my_query["patientinfo.Appointmentinfo.doctornameid"] = doctornameid
        except KeyError:
            doctornameid = ""

        try:
            if request.data['SelectedSpecialistId'] == 0:
                doctorpractitionerid = ""
            else:

                doctorpractitionerid = request.data['SelectedSpecialistId']
                my_query["patientinfo.Appointmentinfo.doctorpractitionerid"] = doctorpractitionerid
        except KeyError:
            doctorpractitionerid = ""

        try:
            if request.data['AppointmentDateforstring'] == 0:
                appointmentdateforstring = ""
            else:

                appointmentdateforstring = request.data['AppointmentDateforstring']
                my_query["patientinfo.Appointmentinfo.appointmentdateforstring"] = appointmentdateforstring
        except KeyError:
            appointmentdateforstring = ""

        # print("query", my_query)

        details = patient_register.find(my_query, {"patientinfo": 1})
        # print("patientinfo :", details)

        my_list = []
        for str1 in details:
            str = json.dumps(str1)
            # print("str1 :", str)

            dict = json.loads(str)

            a = dict['patientinfo']["Appointmentinfo"]
            b = dict['patientinfo']

            print("obj", a)

            for x in a:
                print("xx: s", x)
                if ((x["doctornameid"] == doctornameid or doctornameid == "") and (x["doctorpractitionerid"] == doctorpractitionerid or doctorpractitionerid == "")):
                    if (x["appointmentdate"] == appointmentdateforstring or appointmentdateforstring == ""):
                        my_result = {}
                        my_result["Sno"] = x["sno"]
                        my_result["AppointmentId"] = x["sno"]
                        my_result["PatientId"] = x["patientId"]
                        my_result["PatientName"] = b["LastName"]
                        my_result["DoctorId"] = x["doctornameid"]
                        my_result["DoctorName"] = x["doctorname"]
                        my_result["SpecialistId"] = x["doctorpractitionerid"]
                        my_result["Specialist"] = x["doctorpractitionername"]
                        my_result["Age"] = b["Age"]
                        my_result["Gender"] = b["Gender"]
                        my_result["MobileNo"] = b["CellPhoneNo"]
                        my_result["Reasonformeeting"] = x["Reasonformeetingdoctor"]
                        my_result["AppointmentDate"] = x["date"]

                        print("output values :", my_result)

                        my_list.append(my_result)

        return Response({
            "status": "true",
            "responsecode": 200,
            "message": "Success",
            "data": my_list
        })


class RegistrationDetailReportByID(APIView):
    def get(self, request):
        patientid = request.query_params['id']
        data = int(patientid)
        user = dumps(patient_register.find_one({'_id': data}))

        users = json.loads(user)
        # print("user", users)
        # print("patientinfo", users['patientinfo'])
        # my_lists = []

        my_dats = {}
        my_dats['Id'] = users['_id']
        my_dats['PatientId'] = users['patientid']
        my_dats['PatientInformation'] = users['patientinfo']
        my_dats['FileDetailList'] = users['documentinfo']
        # my_lists.append(my_dats)

        # my_lists = []2022-10-06 16:38:31
        # for k in users:
        #     print("k", k)

        #     # my_dats = {}
        #     # my_dats['Id'] = k['_id']

        #     # my_dats['PatientId'] = k['patientid']
        # #     # my_dats['PatientInformation'] = k['patientinfo']
        #     # my_lists.append(my_dats)

        return Response({
            "status": "true",
            "responsecode": 200,
            "message": "Success",
            "data": my_dats
        })


class VisitedPatientReport(APIView):
    def post(self, request):
        fromdate = request.data['FramDateforstring']

        enddate = request.data['ToDateforstring']

        user = dumps(Patient_visit.find(
            {"visiteddate": {'$gte': fromdate, '$lte': enddate}}))
        # print("user", user)

        data = json.loads(user)
        #print("data", data)

        my_lists = []
        # pk = data['patientinfo']
        # print("pk :", pk)
        # print(my_lists)
        i = 0

        for x in data:
            pk = json.loads(x['patientinfo'])
            i = i+1

            my_data = {}
            my_data['Sno'] = i
            my_data['Id'] = x['_id']
            # di = dict(datetime.fromtimestamp(
            #     int(di['$date'])/1000))
            my_data['VistedDate'] = x['visistedstarttime']

            my_data['VisitedDateforstring'] = datetime.fromisoformat(
                x['visiteddate']).strftime('%d-%b-%Y')
            my_data['PatientId'] = x['patientId']
            my_data['PatientName'] = pk['patient_name']
            my_data['Age'] = pk['age']
            my_data['Gender'] = pk['gender']
            my_data['MobileNo'] = pk['mobileno']
            my_data['Diagnosis'] = pk['reasonformeetingdoctor']
            my_data['Appoinmentid'] = x['appointmentid']
            my_lists.append(my_data)
        return Response({
            "status": "true",
            "responsecode": 200,
            "message": "Success",
            "data": my_lists
        })


class VisitedViewPatientReport(APIView):
    def get(self, request):
        Patient_id = request.query_params['patientid']
        print(Patient_id)
        user = (Patient_visit.find({"patientId": Patient_id}))
        i = 0
        mys_list = []
        for x in user:
            pk = json.loads(x['patientinfo'])

            i = i+1

            my_data = {}
            my_data['Sno'] = i
            my_data['Id'] = x['_id']
            my_data['VisitedDate'] = x['visistedstarttime']
            my_data['VisitedDateforstring'] = datetime.fromisoformat(
                x['visiteddate']).strftime('%d-%b-%Y')
            my_data['PatientId'] = x['patientId']
            my_data['PatientName'] = pk['patient_name']
            my_data['Age'] = pk['age']
            my_data['Gender'] = pk['gender']
            my_data['MobileNo'] = pk['mobileno']
            my_data['Diagnosis'] = pk['reasonformeetingdoctor']
            my_data['Appoinmentid'] = x['appointmentid']
            mys_list.append(my_data)

            # mys_list.append(my_datas)

        return Response({
            "status": "true",
            "responsecode": 200,
            "message": "Success",
            "data": mys_list
        })


class PatientVisitedReportById(APIView):
    def get(self, request):
        patient_id = request.query_params['patientId']
        data = (dumps(Patient_visit.find({"patientId": patient_id})))
        # print(data)

        mydata = json.loads(data)
        for i in mydata:
            # print(i)

            myresult = {}
            myresult["id"] = i['_id']
            myresult["visitedDate"] = i['visiteddate']
            myresult["PatientId"] = i['patientId']
            try:
                myresult["patientinfo"] = i['patientinfo']
            except KeyError:
                myresult["patientinfo"] = ""

            try:
                myresult["temperatureinfo"] = i['temperatureinfo']
            except KeyError:
                myresult["temperatureinfo"] = ""
            try:
                myresult["bpinfo"] = i['bpinfo']
            except KeyError:
                myresult["bpinfo"] = ""
            try:
                myresult["hwinfo"] = i['hwinfo']
            except KeyError:
                myresult["hwinfo"] = ""
            try:
                myresult["spO2info"] = i['spO2info']
            except KeyError:
                myresult["spO2info"] = ""
            myresult["ecginfo"] = ""
            myresult["stestoscopeinfo"] = ""
            myresult["otoscopeinfo1"] = ""
            myresult["otoscopeinfo2"] = ""
            myresult["otoscopeinfo3"] = ""
            myresult["otoscopeinfo4"] = ""
            myresult["otoscopeinfo5"] = ""
            myresult["otoscopeinfo6"] = ""
            myresult["otoscopeinfo7"] = ""
            myresult["otoscopeinfo8"] = ""
            myresult["otoscopeinfo9"] = ""
            myresult["otoscopeinfo10"] = ""
            ts = dict(i['createdAt'])
            tim = datetime.fromtimestamp(int(ts['$date']/1000))

            myresult["createdon"] = tim
            myresult["createdby"] = 'User1'
            ds = dict(i['updatedAt'])
            dim = datetime.fromtimestamp(int(ds['$date']/1000))
            myresult["modifiedon"] = dim
            myresult["modifiedby"] = 'User1'
            try:
                myresult['appoinmentid'] = i['appointmentid']
            except KeyError:
                myresult['appoinmentid'] = ""
            try:
                myresult["patientDescription"] = i['patientDescription']
            except KeyError:
                myresult['patientDescription'] = ""
            try:
                myresult["doctorId"] = i['doctorId']
            except KeyError:
                myresult['doctorId'] = ""

        return Response({
            "status": "true",
            "responsecode": 200,
            "message": "Success",
            "data": myresult
        })


class ViewPatientReport(APIView):
    def post(self, request):
        id = request.data['Id']
        find_id = int(id)

        appoinment_id = request.data['Appointmentid']
        app_id = int(appoinment_id)

        user = dumps(Patient_visit.find(
            {"_id": find_id, "appointmentid": app_id}))

        out = json.loads(user)

        # print(out)

        # mylist = {}
        bfdata = json.loads(
            '{"Id": "","Type": "","Message": "","State": "","Number": "","Date":"","Data" : ""}')

        tei = json.loads(
            '{"Id": "","Type": "","Message": "","State": "","Number": "","Date":"","Data" : ""}')
        hwi = json.loads(
            '{"Id": "","Type": "","Message": "","State": "","Number": "","Date":"","Data" : ""}')
        bpi = json.loads(
            '{"Id": "","Message": "","State": "","Number": "","Date":"","Data" : ""}')

        spi = json.loads(
            '{"Id": "","Message": "","State": "","Number": "","Date":"","Data" : ""}')

        for i in out:

            tmp = dumps(patient_register.find(
                {"patientid": i["patientId"]}, {"patientinfo": 1}))
            out1 = json.loads(tmp)
            # out2 = out1['patientinfo']
            ji = json.dumps(out1)
            ii = json.loads(ji)
            ki = ii[0]["patientinfo"]

            ni = dumps(i)
            mi = json.loads(ni)

            pi = json.loads(i["patientinfo"])
            my_cost = {}
            my_cost["Patient_id"] = pi["patient_id"]
            my_cost["Patient_name"] = pi["patient_name"]
            my_cost["Email"] = pi["email"]
            my_cost["Gender"] = pi["gender"]
            my_cost["Age"] = pi["age"]
            my_cost["mobileno"] = pi["mobileno"]
            my_cost["Reasonformeetingdoctor"] = pi["reasonformeetingdoctor"]
            my_cost["Preexistingdisease"] = pi["preexistingdisease"]

            # bf=json.loads(i["bodyfatinfo"])

            myresult = {}
            myresult["Id"] = mi["_id"]
            myresult["PatientId"] = mi["patientId"]
            myresult["Visiteddate"] = mi["visistedstarttime"]
            myresult["Patientinfo"] = i["patientinfo"]
            myresult["PatientInfos"] = ki

            myresult["VisitedPatientInfo"] = my_cost

            try:
                ti = json.loads(i["temperatureinfo"])
                mytemp = {}
                mytemp["Id"] = ti["id"]
                mytemp["Type"] = ti["type"]
                mytemp["Message"] = ti["message"]
                mytemp["State"] = ti["state"]
                mytemp["Number"] = ti["number"]
                di = ti["date"]
                mytemp["Date"] = datetime.fromtimestamp(int(di))
                mytemp["Data"] = ti["data"]

                myresult["Temperatureinfo"] = i["temperatureinfo"]
                myresult["TemperatureData"] = mytemp
            except KeyError:
                myresult["Temperatureinfo"] = ""
                myresult["TemperatureData"] = tei
            try:
                bi = json.loads(i["bpinfo"])
                mybp = {}
                mybp["Id"] = bi["id"]

                mybp["Message"] = bi["message"]
                mybp["State"] = bi["state"]
                mybp["Number"] = bi["number"]
                di = bi["date"]
                mybp["Date"] = datetime.fromtimestamp(int(di))
                mybp["Data"] = bi["data"]
                myresult["Bpinfo"] = i["bpinfo"]
                myresult["BpData"] = mybp
            except KeyError:
                myresult["Bpinfo"] = ""
                myresult["BpData"] = bpi
            try:
                hi = json.loads(i["hwinfo"])
                myhw = {}
                myhw["Id"] = hi["id"]
                myhw["Type"] = hi["type"]

                myhw["Message"] = hi["message"]
                myhw["State"] = hi["state"]
                myhw["Number"] = hi["number"]
                di = hi["date"]
                myhw["Date"] = datetime.fromtimestamp(int(di))
                myhw["Data"] = hi["data"]

                myresult["Hwinfo"] = i["hwinfo"]
                myresult["HwData"] = myhw
            except KeyError:
                myresult["Hwinfo"] = ""
                myresult["HwData"] = hwi
            try:
                si = json.loads(i["spO2info"])
                mysp = {}
                mysp["Id"] = si["id"]

                mysp["Message"] = si["message"]
                mysp["State"] = si["state"]
                mysp["Number"] = si["number"]
                di = si["date"]
                mysp["Date"] = datetime.fromtimestamp(int(di))
                mysp["Data"] = si["data"]
                myresult["SpO2info"] = i["spO2info"]
                myresult["SpO2Data"] = mysp
            except KeyError:
                myresult["SpO2info"] = ""
                myresult["SpO2Data"] = spi
            try:
                hi = json.loads(i["bodyfatinfo"])
                myresult["Bodyfatinfo"] = i["bodyfatinfo"]
                myresult["BodyfatData"] = hi
            except KeyError:
                myresult["Bodyfatinfo"] = ""
                myresult["BodyfatData"] = bfdata

            myresult["Ecginfo"] = ""
            myresult["EcgData"] = ""
            myresult["Stestoscopeinfo"] = ""
            myresult["StestoscopeData"] = ""
            myresult["stestoscopeinfo"] = ""
            myresult["otoscopeinfo1"] = ""
            myresult["otoscopeinfo1Data"] = ""
            myresult["otoscopeinfo2"] = ""
            myresult["otoscopeinfo3"] = ""
            myresult["otoscopeinfo4"] = ""
            myresult["otoscopeinfo5"] = ""
            myresult["otoscopeinfo6"] = ""
            myresult["otoscopeinfo7"] = ""
            myresult["otoscopeinfo8"] = ""
            myresult["otoscopeinfo9"] = ""
            myresult["otoscopeinfo10"] = ""

            # try:
            #     ot1 = json.loads(i['otoscopeinfo1'])
            #     myresult['Otoscopeinfo1'] = i['otoscopeinfo1']
            #     myresult['Otoscopeinfo1Data'] = ot1
            # except KeyError:
            #     myresult['Otoscopeinfo1'] = ""
            #     myresult['Otoscopeinfo1Data'] = ""

            # try:
            #     ot2 = json.loads(i['otoscopeinfo2'])
            #     myresult['Otoscopeinfo2'] = i['otoscopeinfo2']
            #     myresult['Otoscopeinfo2Data'] = ot2
            # except KeyError:
            #     myresult['Otoscopeinfo2'] = ""
            #     # myresult['Otoscopeinfo2Data'] = 'null'

            # try:
            #     ot3 = json.loads(i['otoscopeinfo3'])
            #     myresult['Otoscopeinfo3'] = i['otoscopeinfo3']
            #     myresult['Otoscopeinfo3Data'] = ot3
            # except KeyError:
            #     myresult['Otoscopeinfo3'] = ""
            #     # myresult['Otoscopeinfo3Data'] = 'null'

            # try:
            #     ot4 = json.loads(i['otoscopeinfo4'])
            #     myresult['Otoscopeinfo4'] = i['otoscopeinfo4']
            #     myresult['Otoscopeinfo4Data'] = ot4
            # except KeyError:
            #     myresult['Otoscopeinfo4'] = ""
            #     # myresult['Otoscopeinfo4Data'] = 'null'

            # try:
            #     ot5 = json.loads(i['otoscopeinfo5'])
            #     myresult['Otoscopeinfo5'] = i['otoscopeinfo5']
            #     myresult['Otoscopeinfo5Data'] = ot5
            # except KeyError:
            #     myresult['Otoscopeinfo5'] = ""
            #     # myresult['Otoscopeinfo5Data'] = 'null'

            # try:
            #     ot6 = json.loads(i['otoscopeinfo6'])
            #     myresult['Otoscopeinfo6'] = i['otoscopeinfo6']
            #     myresult['Otoscopeinfo6Data'] = ot6
            # except KeyError:
            #     myresult['Otoscopeinfo6'] = ""
            #     # myresult['Otoscopeinfo6Data'] = 'null'

            # try:
            #     ot7 = json.loads(i['otoscopeinfo7'])
            #     myresult['Otoscopeinfo7'] = i['otoscopeinfo7']
            #     myresult['Otoscopeinfo7Data'] = ot7
            # except KeyError:
            #     myresult['Otoscopeinfo7'] = ""
            #     # myresult['Otoscopeinfo7Data'] = 'null'

            # try:
            #     ot8 = json.loads(i['otoscopeinfo8'])
            #     myresult['Otoscopeinfo8'] = i['otoscopeinfo8']
            #     myresult['Otoscopeinfo8Data'] = ot8
            # except KeyError:
            #     myresult['Otoscopeinfo8'] = ""
            #     # myresult['Otoscopeinfo8Data'] = 'null'

            # try:
            #     ot9 = json.loads(i['otoscopeinfo9'])
            #     myresult['Otoscopeinfo9'] = i['otoscopeinfo9']
            #     myresult['Otoscopeinfo9Data'] = ot9
            # except KeyError:
            #     myresult['Otoscopeinfo9'] = ""
            #     # myresult['Otoscopeinfo9Data'] = 'null'

            # try:
            #     ot10 = json.loads(i['otoscopeinfo10'])
            #     myresult['Otoscopeinfo10'] = i['otoscopeinfo10']
            #     myresult['Otoscopeinfo10Data'] = ot10
            # except KeyError:
            #     myresult['Otoscopeinfo10'] = ""
            #     # myresult['Otoscopeinfo10Data'] = 'null'

            # # mylist.append(myresult)

        return Response({
            "status": "true",
            "responsecode": 200,
            "message": "success",
            "data": myresult

        })


class PatientReportList(APIView):
    def get(self, request):
        fromdate = request.query_params['fromdate']
        print(fromdate)
        enddate = request.query_params['enddate']
        print(enddate)
        user = dumps(Patient_visit.find(
            {"visiteddate": {'$gte': fromdate, '$lte': enddate}}))
        # print("user", user)

        data = json.loads(user)
        #print("data", data)
        i = 0

        my_lists = []
        for x in data:
            pk = json.loads(x['patientinfo'])
            # pp = range(0, len(x))
            # print("len------", pp)
            i = i+1

            my_data = {}
            my_data['Sno'] = i
            my_data['Id'] = x['_id']
            my_data['VisitedDate'] = x['visistedstarttime']
            my_data['VisitedDateforstring'] = datetime.fromisoformat(
                x['visiteddate']).strftime('%d-%b-%Y')
            my_data['PatientId'] = x['patientId']
            my_data['PatientName'] = pk['patient_name']
            my_data['Age'] = pk['age']
            my_data['Gender'] = pk['gender']
            my_data['MobileNo'] = pk['mobileno']
            my_data['Diagnosis'] = pk['reasonformeetingdoctor']
            my_data['AppointmentId'] = x['appointmentid']
            my_lists.append(my_data)
        return Response({
            "status": "true",
            "responsecode": 200,
            "message": "Success",
            "data": my_lists
        })


class GetPatientPrescription(APIView):
    def get(self, request):
        Patient_id = (request.query_params['patient_id'])
        print(Patient_id)

        Appoinment_id = int(request.query_params['appoinment_id'])
        print(Appoinment_id)

        user = dumps(Patient_visit.find(
            {"patientId": Patient_id, "appointmentid": Appoinment_id}))
        data = json.loads(user)
        my_lists = []
        for x in data:
            pk = json.loads(x['patientDescription'])
            pkk = pk['data']

            my_data = {}
            try:

                my_data['Result'] = pkk['content']
            except KeyError:
                my_data['Result'] = ""

            my_lists.append(my_data)
        return Response({
            "status": "true",
            "responsecode": 200,
            "message": "Success",
            "data": my_lists

        })


class MedicalRecords(APIView):
    def get(self, request):
        Patient_id = request.query_params['patientId']
        No_of_records = request.query_params['noOfRecords']
        Device_name = request.query_params['devices']

        data = (dumps(Patient_visit.find({"patientId": Patient_id}, {
                "temperatureinfo": 1, "bpinfo": 1, "hwinfo": 1, "spO2info": 1, "patientId": 1, "visistedstarttime": 1}).sort("_id", -1)))
        data1 = json.loads(data)
        # print("data", data1)
        mylist = {}

        info = Device_name.split(",")

        i = 1
        for x in info:
            print("device_name", x)

            devicename = x

            # if x == "BP":
            #     devicename = "BP"
            #     print("bp")
            # elif x == "Temperature":
            #     devicename = "Temperature"
            #     print("temperature")
            # elif x == "hwinfo":
            #     devicename = "HeightWeight"
            #     print("hwinfo")
            # elif x == "spO2info":
            #     devicename = "SPO2"
            #     print("spo2 info")

            n = 0
            print("n", n)

            dataset = []

            for j in data1:

                if n < int(No_of_records):
                    print("nn: ", n)

                    try:
                        if x == "Temperature":
                            z = json.loads(j['temperatureinfo'])
                        elif x == "BP":
                            z = json.loads(j["bpinfo"])
                        elif x == "HeightWeight":
                            z = json.loads(j["hwinfo"])
                        elif x == "SPO2":
                            z = json.loads(j["spO2info"])
                        else:
                            z = json.loads(j[x])

                        n += 1
                        my_result = {}
                        my_result["Sno"] = n
                        my_result["Id"] = j["_id"]
                        my_result["DeviceName"] = devicename
                        my_result["PatientId"] = j["patientId"]
                        my_result["VisitedDate"] = j["visistedstarttime"]
                        print("temp", z['data']['content'])

                        my_result["JsonData"] = json.dumps(z['data']['content']).replace(
                            "{", "").replace("\"", "").replace(",", "").replace("}", "").replace(":", "")

                        dataset.append(my_result)

                    except KeyError:
                        print("KeyError", KeyError, j)
                        # pass

            mylist["DeviceData"+str(i)] = dataset
            i = i+1

        for x in range(i, 6):
            mylist["DeviceData"+str(x)] = ""

        return Response({
            "status": "true",
            "responsecode": 200,
            "message": "Success",
            "data": mylist
        })


class PatientVisitedData(APIView):
    def get(self, request):
        Patient_id = request.query_params['patient_id']
        print(Patient_id)
        Appointment_id = int(request.query_params['appointment_id'])
        print(Appointment_id)

        user = dumps(Patient_visit.find(
            {"patientId": Patient_id, "appointmentid": Appointment_id}))
        # print(user)
        outi = json.loads(user)
        # print(out)

        my_line = []

        for x in outi:
            pk = json.loads(x['patientinfo'])
            out = {}
            out['Patient_id'] = pk['patient_id']
            out['Patinet_Name'] = pk['patient_name']
            out['Email'] = pk['email']
            out['Gender'] = pk['gender']
            out['Age'] = pk['age']
            out['Mobileno'] = pk['mobileno']
            out['Reasonformeetingdoctor'] = pk['reasonformeetingdoctor']
            out['Preexistingdisease'] = pk['preexistingdisease']

            temp = {}
            temp['Id'] = ""
            temp['Type'] = ""
            temp['Message'] = ""
            temp['State'] = ""
            temp['Number'] = ""
            temp['Date'] = ""
            temp['Data'] = ""

            bp = {}
            bp['Id'] = ""
            bp['Type'] = ""
            bp['Message'] = ""
            bp['State'] = ""
            bp['Number'] = ""
            bp['Date'] = ""
            bp['Data'] = ""

            hw = {}
            hw['Id'] = ""
            hw['Type'] = ""
            hw['Message'] = ""
            hw['State'] = ""
            hw['Number'] = ""
            hw['Date'] = ""
            hw['Data'] = ""

            sp = {}
            sp['Id'] = ""
            sp['Type'] = ""
            sp['Message'] = ""
            sp['State'] = ""
            sp['Number'] = ""
            sp['Date'] = ""
            sp['Data'] = ""

            body = {}
            body['Id'] = ""
            body['Type'] = ""
            body['Message'] = ""
            body['State'] = ""
            body['Number'] = ""
            body['Date'] = ""
            body['Data'] = ""

            gluco = {}
            gluco['Id'] = ""
            gluco['Type'] = ""
            gluco['Message'] = ""
            gluco['State'] = ""
            gluco['Number'] = ""
            gluco['Date'] = ""
            gluco['Data'] = ""

            my_data = {}
            my_data['Id'] = x["_id"]
            my_data['PatientId'] = x["patientId"]
            my_data['Visiteddate'] = x["visiteddate"]
            my_data['PatientInfo'] = 'null'
            my_data['PatientInfos'] = 'null'
            my_data['VisitedPatientInfo'] = out
            try:
                temper = json.loads(x["temperatureinfo"])
                mytemp = {}
                mytemp["Id"] = temper["id"]
                mytemp["Type"] = temper["type"]
                mytemp["Message"] = temper["message"]
                mytemp["State"] = temper["state"]
                mytemp["Number"] = temper["number"]
                di = temper["date"]
                mytemp["Date"] = datetime.fromtimestamp(int(di))
                mytemp["Data"] = temper["data"]
                my_data["Temperatureinfo"] = x["temperatureinfo"]
                my_data["TemperatureData"] = mytemp
            except KeyError:
                my_data['TempertureInfo'] = ""
                my_data['TempertureData'] = temp

            try:
                bps = json.loads(x['bpinfo'])
                mybp = {}
                mybp["Id"] = bps["id"]

                mybp["Message"] = bps["message"]
                mybp["State"] = bps["state"]
                mybp["Number"] = bps["number"]
                di = bps["date"]
                mybp["Date"] = datetime.fromtimestamp(int(di))
                mybp["Data"] = bps["data"]
                my_data['Bpinfo'] = x['bpinfo']
                my_data['BpData'] = mybp
            except KeyError:
                my_data['Bpinfo'] = ""
                my_data['BpData'] = bp

            try:
                Hw = json.loads(x['hwinfo'])
                myhw = {}
                myhw["Id"] = Hw["id"]
                myhw["Type"] = Hw["type"]

                myhw["Message"] = Hw["message"]
                myhw["State"] = Hw["state"]
                myhw["Number"] = Hw["number"]
                di = Hw["date"]
                myhw["Date"] = datetime.fromtimestamp(int(di))
                myhw["Data"] = Hw["data"]
                my_data['Hwinfo'] = x['hwinfo']

                my_data['HwData'] = myhw
            except KeyError:
                my_data['Hwinfo'] = ""
                my_data['HwData'] = hw

            try:
                Spo2 = json.loads(x['spO2info'])
                mysp = {}
                mysp["Id"] = Spo2["id"]

                mysp["Message"] = Spo2["message"]
                mysp["State"] = Spo2["state"]
                mysp["Number"] = Spo2["number"]
                di = Spo2["date"]
                mysp["Date"] = datetime.fromtimestamp(int(di))
                mysp["Data"] = Spo2["data"]
                my_data['SpO2info'] = x['spO2info']
                my_data['Spo2Data'] = mysp
            except KeyError:
                my_data['SpO2info'] = ""
                my_data["SpO2Data"] = sp

            my_data['Bodyfatinfo'] = ""
            my_data['BodyfatData'] = body
            my_data["otoscopeinfo1"] = ""
            my_data["otoscopeinfo1Data"] = ""
            my_data["otoscopeinfo2"] = ""
            my_data["otoscopeinfo3"] = ""
            my_data["otoscopeinfo4"] = ""
            my_data["otoscopeinfo5"] = ""
            my_data["otoscopeinfo6"] = ""
            my_data["otoscopeinfo7"] = ""
            my_data["otoscopeinfo8"] = ""
            my_data["otoscopeinfo9"] = ""
            my_data["otoscopeinfo10"] = ""

            # try:
            #     ot1 = json.loads(x['otoscopeinfo1'])
            #     my_data['Otoscopeinfo1'] = x['otoscopeinfo1']
            #     my_data['Otoscopeinfo1Data'] = ot1
            # except KeyError:
            #     my_data['Otoscopeinfo1'] = ""

            #     my_data['Otoscopeinfo1Data'] = ""

            # try:
            #     ot2 = json.loads(x['otoscopeinfo2'])
            #     my_data['Otoscopeinfo2'] = x['otoscopeinfo2']
            #     my_data['Otoscopeinfo2Data'] = ot2
            # except KeyError:
            #     my_data['Otoscopeinfo2'] = ""

            # try:
            #     ot3 = json.loads(x['otoscopeinfo3'])
            #     my_data['Otoscopeinfo3'] = x['otoscopeinfo3']
            #     my_data['Otoscopeinfo3Data'] = ot3
            # except KeyError:
            #     my_data['Otoscopeinfo3'] = ""

            # try:
            #     ot4 = json.loads(x['otoscopeinfo4'])
            #     my_data['Otoscopeinfo4'] = x['otoscopeinfo4']
            #     my_data['Otoscopeinfo4Data'] = ot4
            # except KeyError:
            #     my_data['Otoscopeinfo4'] = ""

            # try:
            #     ot5 = json.loads(x['otoscopeinfo5'])
            #     my_data['Otoscopeinfo5'] = x['otoscopeinfo5']
            #     my_data['Otoscopeinfo5Data'] = ot5
            # except KeyError:
            #     my_data['Otoscopeinfo5'] = ""

            # try:
            #     ot6 = json.loads(x['otoscopeinfo6'])
            #     my_data['Otoscopeinfo6'] = x['otoscopeinfo6']
            #     my_data['Otoscopeinfo6Data'] = ot6
            # except KeyError:
            #     my_data['Otoscopeinfo6'] = ""

            # try:
            #     ot7 = json.loads(x['otoscopeinfo7'])
            #     my_data['Otoscopeinfo7'] = x['otoscopeinfo7']
            #     my_data['Otoscopeinfo7Data'] = ot7
            # except KeyError:
            #     my_data['Otoscopeinfo7'] = ""

            # try:
            #     ot8 = json.loads(x['otoscopeinfo8'])
            #     my_data['Otoscopeinfo8'] = x['otoscopeinfo8']
            #     my_data['Otoscopeinfo8Data'] = ot8
            # except KeyError:
            #     my_data['Otoscopeinfo8'] = ""

            # try:
            #     ot9 = json.loads(x['otoscopeinfo9'])
            #     my_data['Otoscopeinfo9'] = x['otoscopeinfo9']
            #     my_data['Otoscopeinfo9Data'] = ot9
            # except KeyError:
            #     my_data['Otoscopeinfo9'] = ""

            # try:
            #     ot10 = json.loads(x['otoscopeinfo10'])
            #     my_data['Otoscopeinfo10'] = x['otoscopeinfo10']
            #     my_data['Otoscopeinfo10Data'] = ot10
            # except KeyError:
            #     my_data['Otoscopeinfo10'] = ""

            my_data['Glucometerinfo'] = ""
            my_data['GlucometerData'] = gluco

            my_line.append(my_data)

        return Response({
            "status": "true",
            "message": "Success",
            "responsecode": 200,
            "data": my_line
        })


class GetPatientvisitedStestoscopeData(APIView):
    def get(self, request):
        Patient_id = request.query_params['patient_id']
        print(Patient_id)
        Appointment_id = int(request.query_params['appointment_id'])
        print(Appointment_id)

        user = dumps(Patient_visit.find(
            {"patientId": Patient_id, "appointmentid": Appointment_id}))
        # print(user)
        out = json.loads(user)
        # print(out)

        my_line = []

        for x in out:
            pk = json.loads(x['patientinfo'])
            ball = {}
            ball['Patient_id'] = pk['patient_id']
            ball['Patinet_Name'] = pk['patient_name']
            ball['Email'] = pk['email']
            ball['Gender'] = pk['gender']
            ball['Age'] = pk['age']
            ball['Mobileno'] = pk['mobileno']
            ball['Reasonformeetingdoctor'] = pk['reasonformeetingdoctor']
            ball['Preexistingdisease'] = pk['preexistingdisease']

            my_data = {}
            my_data['Id'] = x["_id"]
            my_data['PatientId'] = x["patientId"]
            my_data['Visiteddate'] = x["visiteddate"]
            my_data['PatientInfo'] = ""
            my_data['PatientInfos'] = ""
            my_data['VisitedPatientInfo'] = ball

            my_data['Stestoscopeinfo'] = ""
            my_data['StestoscopeData'] = ""
            my_line.append(my_data)

        return Response({
            "status": "true",
            "message": "Success",
            "responsecode": 200,
            "data": my_line
        })


class GetPatientvisitedECGDataBy(APIView):
    def get(self, request):
        Patient_id = request.query_params['patient_id']
        print(Patient_id)
        Appointment_id = int(request.query_params['appointment_id'])
        print(Appointment_id)

        user = dumps(Patient_visit.find(
            {"patientId": Patient_id, "appointmentid": Appointment_id}))
        # print(user)
        out = json.loads(user)
        # print(out)

        my_line = []

        for x in out:
            pk = json.loads(x['patientinfo'])
            ball = {}
            ball['Patient_id'] = pk['patient_id']
            ball['Patinet_Name'] = pk['patient_name']
            ball['Email'] = pk['email']
            ball['Gender'] = pk['gender']
            ball['Age'] = pk['age']
            ball['Mobileno'] = pk['mobileno']
            ball['Reasonformeetingdoctor'] = pk['reasonformeetingdoctor']
            ball['Preexistingdisease'] = pk['preexistingdisease']

            my_data = {}
            my_data['Id'] = x["_id"]
            my_data['PatientId'] = x["patientId"]
            my_data['Visiteddate'] = x["visiteddate"]
            my_data['PatientInfo'] = ""
            my_data['PatientInfos'] = ""
            my_data['VisitedPatientInfo'] = ball

            # try:
            #     ecg = json.loads(x['ecginfo'])
            #     my_data['Ecginfo'] = x['ecginfo']
            #     my_data['EcgData'] = ecg
            # except KeyError:
            my_data['Ecginfo'] = ""
            my_data['EcgData'] = ""

            my_line.append(my_data)

        return Response({
            "status": "true",
            "message": "Success",
            "responsecode": 200,
            "data": my_line
        })


class GetPatientvisitedEMGDataBy(APIView):
    def get(self, request):
        Patient_id = request.query_params['patient_id']
        print(Patient_id)
        Appointment_id = int(request.query_params['appointment_id'])
        print(Appointment_id)

        user = dumps(Patient_visit.find(
            {"patientId": Patient_id, "appointmentid": Appointment_id}))
        # print(user)
        out = json.loads(user)
        # print(out)

        my_line = []

        for x in out:
            pk = json.loads(x['patientinfo'])

            #print('pk info :', pk)
            ball = {}
            ball['Patient_id'] = pk['patient_id']
            ball['Patinet_Name'] = pk['patient_name']
            ball['Email'] = pk['email']
            ball['Gender'] = pk['gender']
            ball['Age'] = pk['age']
            ball['Mobileno'] = pk['mobileno']
            ball['Reasonformeetingdoctor'] = pk['reasonformeetingdoctor']
            ball['Preexistingdisease'] = pk['preexistingdisease']

            my_data = {}
            my_data['Id'] = x["_id"]
            my_data['PatientId'] = x["patientId"]
            my_data['Visiteddate'] = x["visiteddate"]
            my_data['PatientInfo'] = ""
            my_data['PatientInfos'] = ""
            my_data['VisitedPatientInfo'] = ball

            try:

                emg = dumps(x['emginfo'])
                emg1 = json.loads(emg)
                my_data['Emginfo'] = x['emginfo']
                my_data['EmgData'] = emg1
            except KeyError:
                my_data['Emginfo'] = ""
                my_data['EmgData'] = ""

            my_line.append(my_data)

        return Response({
            "status": "true",
            "message": "Success",
            "responsecode": 200,
            "data": my_line
        })


class GetPatientvisitedGSRDataBy(APIView):
    def get(self, request):
        Patient_id = request.query_params['patient_id']
        print(Patient_id)
        Appointment_id = int(request.query_params['appointment_id'])
        print(Appointment_id)

        user = dumps(Patient_visit.find(
            {"patientId": Patient_id, "appointmentid": Appointment_id}))
        # print(user)
        out = json.loads(user)
        # print(out)

        my_line = []

        for x in out:
            pk = json.loads(x['patientinfo'])
            #print('pk info :', pk)
            ball = {}
            ball['Patient_id'] = pk['patient_id']
            ball['Patinet_Name'] = pk['patient_name']
            ball['Email'] = pk['email']
            ball['Gender'] = pk['gender']
            ball['Age'] = pk['age']
            ball['Mobileno'] = pk['mobileno']
            ball['Reasonformeetingdoctor'] = pk['reasonformeetingdoctor']
            ball['Preexistingdisease'] = pk['preexistingdisease']

            my_data = {}
            my_data['Id'] = x["_id"]
            my_data['PatientId'] = x["patientId"]
            my_data['Visiteddate'] = x["visiteddate"]
            my_data['PatientInfo'] = ""
            my_data['PatientInfos'] = ""
            my_data['VisitedPatientInfo'] = ball

            try:
                gsr = json.loads(x['gsrinfo'])
                my_data['Gsrinfo'] = x['gsrinfo']
                my_data['GsrData'] = gsr
            except KeyError:
                my_data['Gsrinfo'] = ""
                my_data['GsrData'] = ""

            my_line.append(my_data)

        return Response({
            "status": "true",
            "message": "Success",
            "responsecode": 200,
            "data": my_line
        })
