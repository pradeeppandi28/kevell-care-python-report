"""kevelsproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from django.urls import path
from kevelapp import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('ReportsMS/api/Report/GetAppointmentData',
         views.GetAppointmentData.as_view()),
    path('ReportsMS/api/Report/GetAllPatientRegistrations',
         views.GetAllPatientRegistrations.as_view()),
    path('ReportsMS/api/Report/RegistrationDetailReportBy',
         views.RegistrationDetailReportByID.as_view()),
    path('ReportsMS/api/Report/VisitedPatientReport',
         views.VisitedPatientReport.as_view()),
    path('ReportsMS/api/Report/VisitedViewPatientReport',
         views.VisitedViewPatientReport.as_view()),
    path('ReportsMS/api/Report/ViewPatientReport',
         views.ViewPatientReport.as_view()),
    path('ReportsMS/api/Report/PatientVisitedReportById',
         views.PatientVisitedReportById.as_view()),
    path('ReportsMS/api/Report/PatientReportList',
         views.PatientReportList.as_view()),
    path('ReportsMS/api/Report/GetPatientPrescription',
         views.GetPatientPrescription.as_view()),
    path('ReportsMS/api/Report/MedicalRecords', views.MedicalRecords.as_view()),
    path('ReportsMS/api/Report/PatientVisitedData',
         views.PatientVisitedData.as_view()),
    path('ReportsMS/api/Report/GetPatientvisitedStestoscopeData',
         views.GetPatientvisitedStestoscopeData.as_view()),
    path('ReportsMS/api/Report/GetPatientvisitedECGDataBy',
         views.GetPatientvisitedECGDataBy.as_view()),
    path('ReportsMS/api/Report/GetPatientvisitedEMGDataBy',
         views.GetPatientvisitedEMGDataBy.as_view()),
    path('ReportsMS/api/Report/GetPatientvisitedGSRDataBy',
         views.GetPatientvisitedGSRDataBy.as_view()),


]
